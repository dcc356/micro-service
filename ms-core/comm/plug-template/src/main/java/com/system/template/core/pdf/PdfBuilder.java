package com.system.template.core.pdf;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfBuilder extends PdfPageEventHelper {

	// 当当前页面开始加载时，触发(页眉)
	@Override
	public void onStartPage(PdfWriter writer, Document document) {
		super.onStartPage(writer, document);
	}

	// 当当前页面初始化完成，切入document之前触发(页脚)
	@Override
	public void onEndPage(PdfWriter writer, Document document) {

		Rectangle rect = new Rectangle(document.getPageSize().getWidth(), 25, 0, 0);
		// ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
		// new Phrase(String.format("      ", writer.getPageNumber())), (rect.getLeft() + rect.getRight()) / 2,
		// rect.getBottom() - 18, 0);
		ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_CENTER,
				new Phrase(String.format("- %d -", writer.getPageNumber())), 
				(rect.getLeft() + rect.getRight()) / 2,
				rect.getBottom() - 18,
				0);
	}
}