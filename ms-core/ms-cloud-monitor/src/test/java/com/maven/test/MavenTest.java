package com.maven.test;

import java.util.ArrayList;
import java.util.List;

import com.module.comm.utils.MavenUtil;
import com.system.comm.utils.FrameJsonUtil;
import com.system.handle.model.ResponseFrame;

public class MavenTest {
	

	public static void main(String[] args) {
		String pomPath = "D:\\data\\monitor\\project\\spider-server\\pom.xml";
		String mavenHomePath = "C:\\Users\\Administrator\\MySoft\\apache-maven-3.0.1";
		List<String> commands = new ArrayList<String>();
		commands.add("clean");
		commands.add("install");
		ResponseFrame frame = MavenUtil.execute(pomPath, mavenHomePath, commands);
		System.out.println(FrameJsonUtil.toString(frame));
	}

}
