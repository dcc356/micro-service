package com.module.admin.sys.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.module.admin.BaseController;
import com.module.admin.sys.pojo.SysUser;
import com.module.admin.sys.pojo.SysUserAtt;
import com.module.admin.sys.service.SysUserAttService;
import com.system.handle.model.ResponseCode;
import com.system.handle.model.ResponseFrame;

/**
 * sys_config的Controller
 * @author yuejing
 * @date 2016-10-19 13:50:15
 * @version V1.0.0
 */
@Controller
public class SysUserAttController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SysUserAttController.class);

	@Autowired
	private SysUserAttService sysUserAttService;
	

	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/sysUserAtt/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
		SysUserAtt sysUserAtt) {
		ResponseFrame frame = null;
		try {
			frame = sysUserAttService.pageQuery(sysUserAtt);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}

	/**
	 * 保存
	 * @return
	 */
	@RequestMapping(value = "/sysUserAtt/f-json/save")
	@ResponseBody
	public void save(HttpServletRequest request, HttpServletResponse response,
			SysUserAtt sysUserAtt) {
		ResponseFrame frame = null;
		try {
			SysUser user = getSessionUser(request);
			sysUserAtt.setUserId(user.getUserId());
			frame = sysUserAttService.save(sysUserAtt);
		} catch (Exception e) {
			LOGGER.error("保存异常: " + e.getMessage(), e);
			frame = new ResponseFrame(ResponseCode.FAIL);
		}
		writerJson(response, frame);
	}
	
	/**
	 * 删除
	 * @return
	 */
	@RequestMapping(value = "/sysUserAtt/f-json/delete")
	@ResponseBody
	public void delete(HttpServletRequest request, HttpServletResponse response,
		String type, String typeNo) {
		ResponseFrame frame = null;
		try {
			SysUser user = getSessionUser(request);
			frame = sysUserAttService.delete(user.getUserId(), type, typeNo);
		} catch (Exception e) {
			LOGGER.error("删除异常: " + e.getMessage(), e);
			frame = new ResponseFrame();
			frame.setCode(ResponseCode.FAIL.getCode());
		}
		writerJson(response, frame);
	}
}
