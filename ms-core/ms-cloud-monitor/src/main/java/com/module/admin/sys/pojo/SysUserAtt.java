package com.module.admin.sys.pojo;

import java.io.Serializable;
import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.system.comm.model.BaseEntity;

/**
 * sys_user_att实体
 * @author autoCode
 * @date 2018-06-12 09:59:57
 * @version V1.0.0
 */
@Alias("sysUserAtt")
@SuppressWarnings("serial")
@JsonInclude(Include.NON_NULL)
public class SysUserAtt extends BaseEntity implements Serializable {
	//用户编号
	private Integer userId;
	//类型[10项目]
	private String type;
	//类型编码
	private String typeNo;
	//创建时间
	private Date createTime;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getTypeNo() {
		return typeNo;
	}
	public void setTypeNo(String typeNo) {
		this.typeNo = typeNo;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}