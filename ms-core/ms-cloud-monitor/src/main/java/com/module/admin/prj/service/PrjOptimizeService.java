package com.module.admin.prj.service;

import org.springframework.stereotype.Component;

import com.module.admin.prj.pojo.PrjOptimize;
import com.system.handle.model.ResponseFrame;

/**
 * prj_optimize的Service
 * @author autoCode
 * @date 2018-05-31 15:51:51
 * @version V1.0.0
 */
@Component
public interface PrjOptimizeService {
	
	/**
	 * 保存或修改
	 * @param prjOptimize
	 * @return
	 */
	public ResponseFrame saveOrUpdate(PrjOptimize prjOptimize);
	
	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public PrjOptimize get(Integer id);

	/**
	 * 分页获取对象
	 * @param prjOptimize
	 * @return
	 */
	public ResponseFrame pageQuery(PrjOptimize prjOptimize);
	
	/**
	 * 根据id删除对象
	 * @param id
	 * @return
	 */
	public ResponseFrame delete(Integer id);

	public PrjOptimize getByPrjIdUrl(Integer prjId, String url);
}